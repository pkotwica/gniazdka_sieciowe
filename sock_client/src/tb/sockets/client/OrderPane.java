package tb.sockets.client;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.*;
import java.awt.geom.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;


import javax.swing.JPanel;




public class OrderPane extends JPanel implements MouseListener, Runnable{

	/**
	 * Create the panel.
	 */
	
	static final int dl = 150;
	ArrayList<Shape> znaki = new ArrayList();
	int [][] wyniki = new int[3][3];
	boolean czyKoniec = false;
	boolean czyStart = false;
	boolean czyKolko = true;
	boolean czyjRuch = true;
	
	ServerSocket serv;
	Socket sock;
	BufferedReader inm;
	PrintWriter outm;
	
	
	
	
	
	public OrderPane() {
		setBounds(145, 14, 620, 620);
		setBackground(Color.WHITE);
		
		for(int i=0; i<3; i++)
			for(int j=0; j<3; j++)
				wyniki[i][j] = 0;
		
		addMouseListener(this);
	}
	
	
	
	
	
	public void creatServer(String socket) {
		
		try {
			
			String[] wybor = {"ja", "przeciwnik"};
			int response =JOptionPane.showOptionDialog(null, "Jako serwer wybierz kto ma wykonac pierwszy ruch: ", "", 0, JOptionPane.INFORMATION_MESSAGE, null, wybor, wybor[1]);
			if(response == 1)
				czyjRuch = false;
			
			
			serv = new ServerSocket(Integer.parseInt(socket));
			sock = serv.accept();
			
			
			DataInputStream in = new DataInputStream(sock.getInputStream());
			this.inm = new BufferedReader(new InputStreamReader(in));
			
			outm = new PrintWriter(sock.getOutputStream(), true);
			
			if(czyjRuch)
				outm.println("JA");
			else
				outm.println("TY");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Blad! Nie udalo sie utworzyc serwera.", "ERROR",JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		
		czyStart = true;
		
	}
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		String in = "";
		
		while(true) {
			
			try {
					
					in = this.inm.readLine();
					
					if(in.startsWith("X")) {
						
						int ruch = Integer.parseInt(in.substring(1));
						
						if(!czyjRuch) {
							checkMessage(ruch);
							Winner(verifyWinner());
						}
						
					}
					
	
					

					
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null, "Przeciwnik nie chcial kontynuaowac gry lub utracono niespodzieanie polaczenie.", "ERROR",JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
				System.exit(0);
			}
			
			
			try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
			
			
			
		}
		
		
		
	}
	
	
	
	
	private void checkMessage(int ruch) {
		if(ruch >= 0 && ruch <= 8)
			addSign(ruch, "przeciwnik");
		else
			outm.println("ERR");
	}
	
	
	
	public void addSign(int p, String gracz) {
		
		int x=0;
		int y=0;
		
		switch(p) {
		case 0: 
			x = 105;
			y = 105;
			break;
		case 1: 
			x = 305;
			y = 105;
			break;
		case 2: 
			x = 505;
			y = 105;
			break;
		case 3: 
			x = 105;
			y = 305;
			break;
		case 4: 
			x = 305;
			y = 305;
			break;
		case 5: 
			x = 505;
			y = 305;
			break;
		case 6: 
			x = 105;
			y = 505;
			break;
		case 7: 
			x = 305;
			y = 505;
			break;
		case 8: 
			x = 505;
			y = 505;
			break;		
		}
			
		
		
		paintSign(x, y, gracz);
		
		
		
	}
	
	
	
	
	
	public void paintSign(int x, int y, String gracz) {
		
		if(czyStart)
		if(gracz.equals("przeciwnik") ){
			if(!czyKolko) {
				
				if(!setTab(x, y, 1))
						return;
				znaki.add(new Ellipse2D.Float(x-(dl/2), y-(dl/2), dl, dl));
			}
			else {
				if(!setTab(x, y, 2))
					return;
				znaki.add(new Line2D.Float(x-(dl/2), y-(dl/2), x+(dl/2), y+(dl/2)));
				znaki.add(new Line2D.Float(x-(dl/2), y+(dl/2), x+(dl/2), y-(dl/2)));
				
			}	
		}
		else
			if(czyKolko) {
				if(!setTab(x, y, 1))
					return;
				znaki.add(new Ellipse2D.Float(x-(dl/2), y-(dl/2), dl, dl));
			}
			else {
				if(!setTab(x, y, 2))
					return;
				znaki.add(new Line2D.Float(x-(dl/2), y-(dl/2), x+(dl/2), y+(dl/2)));
				znaki.add(new Line2D.Float(x-(dl/2), y+(dl/2), x+(dl/2), y-(dl/2)));
			}
		
		
		repaint();

		
	}
	
	
	

	public void send(int x, int y) {
		int p = -10;
		
		if(x == 105 && y == 105)
			p = 0;
		
		if(x == 305 && y == 105)
			p = 1;
		
		if(x == 505 && y == 105)
			p = 2;
		
		if(x == 105 && y == 305)
			p = 3;
		
		if(x == 305 && y == 305)
			p = 4;
		
		if(x == 505 && y == 305)
			p = 5;
		
		if(x == 105 && y == 505)
			p = 6;
		
		if(x == 305 && y == 505)
			p = 7;
		
		if(x == 505 && y == 505)
			p = 8;
		
	
		outm.println("O"+p);
		
	
	
	}
	
	
	
	
	
	

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D comp = (Graphics2D) g;
		
		comp.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		comp.setStroke(new BasicStroke( 5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		
		Line2D linia1 = new Line2D.Float(210, 10, 210, 610);
		Line2D linia2 = new Line2D.Float(410, 10, 410, 610);
		Line2D linia3 = new Line2D.Float(10, 210, 610, 210);
		Line2D linia4 = new Line2D.Float(10, 410, 610, 410);
		
		comp.draw(linia1);
		comp.draw(linia2);
		comp.draw(linia3);
		comp.draw(linia4);
		
		for(Shape znak: znaki) 
			comp.draw(znak);
			
		
		
		
	}
	
	
		
	
	
	
	
	protected boolean setTab(int p, int q, int znak) {
		
		int i = 0;
		int j = 0;
		
		if(p == 105)
			i = 0;
		
		if(p == 305)
			i = 1;
		
		if(p == 505)
			i = 2;
		
		if(q == 105)
			j = 0;
		
		if(q == 305)
			j = 1;
		
		if(q == 505)
			j = 2;
		
		
		
		
		if(wyniki[i][j] == 0) {
			if(znak == 1) {
				wyniki[i][j] = 1;
				
				if(czyjRuch)
					czyjRuch = false;
				else
					czyjRuch = true;
				return true;
			}
			else {
				wyniki[i][j] = 2;
				
				if(czyjRuch)
					czyjRuch = false;
				else
					czyjRuch = true;
				return true;
			}
			
			
					
		}	
		else {
			JOptionPane.showMessageDialog(null, "Zly ruch!", "ERROR",JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		
	}
	
	
	
	
	
	protected int setPoint(int p) {
		
		int q;
		
		if(p > 10 && p < 200)
			q = 105;
		else
			q = -10;
		
		if(p > 210 && p < 400)
			q = 305;
		
		if(p > 410 && p < 600)
			q = 505;
		
		
		return q;
	}
	
	
	
	
	
	
	protected int verifyWinner() {
		//0-brak wygranej
		//1-wygrana kolko
		//2-wygrna kryzyk
		
		
		for(int i=0; i<3; i++) {
			
			if(wyniki[i][i] == 1 && wyniki[i][(i+1)%3] == 1 && wyniki[i][(i+2)%3] == 1)
				return 1;
			
			if(wyniki[i][i] == 1 && wyniki[(i+1)%3][i] == 1 && wyniki[(i+2)%3][i] == 1)
				return 1;
			
			if(wyniki[i][i] == 2 && wyniki[i][(i+1)%3] == 2 && wyniki[i][(i+2)%3] == 2)
				return 2;
			
			if(wyniki[i][i] == 2 && wyniki[(i+1)%3][i] == 2 && wyniki[(i+2)%3][i] == 2)
				return 2;
			
		}
		
		
		
		
		
		if(wyniki[0][0] == 1 && wyniki[1][1] == 1 && wyniki[2][2] == 1)
			return 1;
		
		if(wyniki[0][0] == 2 && wyniki[1][1] == 2 && wyniki[2][2] == 2)
			return 2;
		
		if(wyniki[0][2] == 1 && wyniki[1][1] == 1 && wyniki[2][0] == 1)
			return 1;
		
		if(wyniki[0][2] == 2 && wyniki[1][1] == 2 && wyniki[2][0] == 2)
			return 2;
			
			

		
		return 0;
	}
	
	
	
	
	
	protected void Winner(int w) {
		
		
		if(w == 1) {
			czyKoniec = true;
			
			JOptionPane.showMessageDialog(null, "KOLKO WYGRYWA!!!", "",JOptionPane.INFORMATION_MESSAGE);
		}
			
		
		if(w == 2) {
			czyKoniec = true;
			
			JOptionPane.showMessageDialog(null, "KZRYZYK WYGRYWA!!!", "",JOptionPane.INFORMATION_MESSAGE);	
		}
		
		boolean flag0 = true;
		
		if(w == 0) {
			
			for(int i=0; i<3; i++) 
				for(int j=0; j<3; j++)
					if(wyniki[i][j] == 0) 
						flag0 = false;	
					

			if(flag0) {
				JOptionPane.showMessageDialog(null, "REMIS!!!", "",JOptionPane.INFORMATION_MESSAGE);	
				czyKoniec = true;
			}
				
			
		}
		

		if(czyKoniec == true) {
			int response = JOptionPane.showConfirmDialog(null, "Czy chcesz zaczac gre od poczatku?", "", JOptionPane.YES_NO_OPTION,JOptionPane.INFORMATION_MESSAGE);
			
			if(response == JOptionPane.YES_OPTION) {
				czyKoniec = false;
				
				for(int i=0; i<3; i++)
					for(int j=0; j<3; j++)
						wyniki[i][j] = 0;
				
				znaki.clear();
				
				repaint();
			} else {
				try {
					serv.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {serv.close();} catch (IOException e) {}
				System.exit(0);

			}
				
		}
		
		
		
	}
	
	
	
	
	
	
	public String czyjRuch() {
		
		if(czyjRuch)
			return "Twoj ruch";
		else
			return "Ruch przeciwnika";
	}
	
	public int getIP() {
		
		return serv.getLocalPort();
		
	}
	
	
	public void init(int wybor) {
		if(wybor == 1)
			czyKolko = false;
		
	}
	
	

	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
		
		
	}
	
	
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		if(czyjRuch) {
			if(!czyKoniec && czyStart) {
				
				if(setPoint(e.getX()) != -10 && setPoint(e.getY()) != -10) {
					
					int x = setPoint(e.getX());
					int y = setPoint(e.getY());
					
					
					if(e.getButton() == MouseEvent.BUTTON1) 
						paintSign(x, y, "ja");
					
					
					send(x, y);
					Winner(verifyWinner());
				
				}		
			}	
		}
		
		else
			JOptionPane.showMessageDialog(null, "Zly ruch!", "ERROR",JOptionPane.ERROR_MESSAGE);
			

	}
	
	
	
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	
}
