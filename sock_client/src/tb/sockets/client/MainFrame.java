package tb.sockets.client;

import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParseException;
import java.text.ParsePosition;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

import javafx.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JFormattedTextField;
import java.awt.Color;





public class MainFrame extends JFrame implements Runnable {

	private JPanel contentPane;
	private String sock;
	JFormattedTextField frmtdtxtfldIp;
	JFormattedTextField frmtdtxtfldXxxx;
	JLabel lblNotConnected;
	JLabel czyjRuch;
	JLabel ip;
	OrderPane panel;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					
					new Thread(frame).start();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		
		
		
		
		super("Kolko i Krzyzyk");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 700);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		
		ip = new JLabel("IP: ", JLabel.CENTER);
		ip.setOpaque(true);
		ip.setBounds(10, 14, 100, 14);
		contentPane.add(ip);
		
		/*
		JLabel lblHost = new JLabel("Host:");
		lblHost.setBounds(10, 14, 30, 14);
		contentPane.add(lblHost);
		
		
		try {
			frmtdtxtfldIp = new JFormattedTextField(new MaskFormatter("###.###.###.###"));
			frmtdtxtfldIp.setBounds(43, 11, 90, 20);
			frmtdtxtfldIp.setText("xxx.xxx.xxx.xxx");
			contentPane.add(frmtdtxtfldIp);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		
		JButton btnConnect = new JButton("Create Server");
		btnConnect.setBounds(10, 70, 123, 23);
		btnConnect.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				// TODO Auto-generated method stub
				
				
				JOptionPane.showMessageDialog(null, "Zaczekaj na gracza.", "",JOptionPane.INFORMATION_MESSAGE);
				sock =  frmtdtxtfldXxxx.getText();
				panel.creatServer(sock);	
				
				new Thread(panel).start();
				
				ip.setText("IP: "+panel.getIP());
				lblNotConnected.setText("Connected");
				lblNotConnected.setBackground(Color.GREEN);
			}
		});
		
		
		contentPane.add(btnConnect);
		
		frmtdtxtfldXxxx = new JFormattedTextField();
		frmtdtxtfldXxxx.setText("xxxx");
		frmtdtxtfldXxxx.setBounds(43, 39, 90, 20);
		contentPane.add(frmtdtxtfldXxxx);
		
		JLabel lblPort = new JLabel("Port:");
		lblPort.setBounds(10, 42, 30, 14);
		contentPane.add(lblPort);
		
		panel = new OrderPane();
		contentPane.add(panel);
		
		lblNotConnected = new JLabel("Not Connected", JLabel.CENTER);
		lblNotConnected.setForeground(new Color(255, 255, 255));
		lblNotConnected.setBackground(new Color(128, 128, 128));
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBounds(10, 104, 123, 23);
		contentPane.add(lblNotConnected);
		
		czyjRuch = new JLabel("Wybierz", JLabel.CENTER);
		czyjRuch.setOpaque(true);
		czyjRuch.setBounds(10, 230, 123, 23);
		contentPane.add(czyjRuch);
	
	
		String[] wybor = {"Kolko", "Krzyzyk"};
		panel.init(JOptionPane.showOptionDialog(null, "Wybierz czym chcesz grac: ", "", 0, JOptionPane.INFORMATION_MESSAGE, null, wybor, wybor[1]));
	
	
	}
	
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(true) {
		
			czyjRuch.setText(panel.czyjRuch());
			
			
			try {
	            Thread.sleep(10);
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
			
		}
		
		
		
	
		
	}
	
	
	
	
	
	
}
